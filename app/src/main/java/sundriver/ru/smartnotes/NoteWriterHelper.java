package sundriver.ru.smartnotes;

import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import sundriver.ru.smartnotes.models.Note;

/**
 * Created by Eugen on 01.03.2017.
 */

public class NoteWriterHelper{
    public static final String PATH="/SmarNotes";
    public static boolean isExternalStorageWritable(){
        String state= Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)){
            return true;
        }
        return false;
    }

    public static String writeNote(Note note){
        String res=null;
        String root=Environment.getExternalStorageDirectory().toString();
        File dirPath=new File(root+PATH);
        if(!dirPath.exists()){
            dirPath.mkdir();
        }
        File filePath=new File(dirPath,note.getTitle()+".txt");
        if (filePath.exists())
            filePath=generateFilename(note.getTitle(),dirPath);
        try {
            FileOutputStream outputStream=new FileOutputStream(filePath);
            BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
            bufferedWriter.write(Utils.generateMessage(note));

            bufferedWriter.flush();
            bufferedWriter.close();
            res=PATH+"/"+filePath.getName();
        } catch (IOException e) {
        }
        return res;

    }

    private static File generateFilename(String title,File path){
        boolean flag=true;
        int i=1;
        String filename;
        File filePath=null;
        while(flag){
            filename=title+"("+i+").txt";
            filePath=new File(path,filename);
            flag=filePath.exists();
            i++;
        }
        return filePath;
    }
}
