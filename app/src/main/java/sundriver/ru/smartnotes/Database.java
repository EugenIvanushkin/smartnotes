package sundriver.ru.smartnotes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import sundriver.ru.smartnotes.models.Note;

/**
 * Created by Eugen on 22.02.2017.
 */

public class Database {
    public static final String DB_NAME="smart_notes";
    public static final String DB_TABLE="notes";
    public static final String ID_COLUMN="_id";
    public static final String TITLE_COLUMN="title";
    public static final String NOTE_COLUMN="note";
    public static final String PHOTO_COLUMN="photo_path";
    public static final String PRIORITY_COLUMN="priority";
    public static final String COORDINATES_COLUMN="coordinates";

    private DatabaseHelper databaseHelper;
    private SQLiteDatabase database;

    public Database(Context context){
        databaseHelper=new DatabaseHelper(context,DB_NAME,null,3);
        database=databaseHelper.getWritableDatabase();
    }

    public Cursor getAllNotes(){
        Cursor cursor=database.query(DB_TABLE,new String[]{PHOTO_COLUMN,TITLE_COLUMN,ID_COLUMN,COORDINATES_COLUMN,PRIORITY_COLUMN},null,null,null,null,null);
        cursor.moveToFirst();
        return cursor;
    }


    public void addNote(Note note){
        ContentValues cv=getContentValues(note);
        database.insert(DB_TABLE,null,cv);

    }

    public void updateNote(Note note){
        ContentValues cv=getContentValues(note);
        database.update(DB_TABLE,cv,ID_COLUMN+"="+note.getId(),null);
    }

    public void deleteNote(Long id){
        database.delete(DB_TABLE,ID_COLUMN+"="+id,null);
    }

    public Note getNote(Long id){
        Cursor cursor=database.query(DB_TABLE,null,ID_COLUMN+"="+id,null,null,null,null);
        cursor.moveToFirst();
        return getNote(cursor);
    }
    private ContentValues getContentValues(Note note){
        ContentValues cv=new ContentValues();
        if(note.getId()!=null){
            cv.put(ID_COLUMN,note.getId());
        }
        cv.put(TITLE_COLUMN,note.getTitle());
        cv.put(NOTE_COLUMN,note.getNote());
        cv.put(PRIORITY_COLUMN,note.getPriority());
        cv.put(PHOTO_COLUMN,note.getPhotoPath());
        cv.put(COORDINATES_COLUMN,note.getCoordinates());
        return cv;
    }

    private Note getNote(Cursor cursor){
        Note note=new Note();
        note.setId(cursor.getLong(cursor.getColumnIndex(ID_COLUMN)));
        note.setTitle(cursor.getString(cursor.getColumnIndex(TITLE_COLUMN)));
        note.setNote(cursor.getString(cursor.getColumnIndex(NOTE_COLUMN)));
        note.setPhotoPath(cursor.getString(cursor.getColumnIndex(PHOTO_COLUMN)));
        note.setPriority(cursor.getInt(cursor.getColumnIndex(PRIORITY_COLUMN)));
        note.setCoordinates(cursor.getString(cursor.getColumnIndex(COORDINATES_COLUMN)));
        return note;
    }
    private class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table "+DB_TABLE+"("+ID_COLUMN+" integer primary key autoincrement," +
                    TITLE_COLUMN+" text," +
                    NOTE_COLUMN+" text," +
                    PHOTO_COLUMN+" text," +
                    COORDINATES_COLUMN+" text,"+
                    PRIORITY_COLUMN+" integer )");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE);
            onCreate(db);
        }
    }
}
