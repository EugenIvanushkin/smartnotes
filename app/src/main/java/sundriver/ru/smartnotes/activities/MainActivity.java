package sundriver.ru.smartnotes.activities;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import sundriver.ru.smartnotes.CustomCursorLoader;
import sundriver.ru.smartnotes.Database;
import sundriver.ru.smartnotes.NotesCursorAdapter;
import sundriver.ru.smartnotes.R;
import sundriver.ru.smartnotes.Utils;
import sundriver.ru.smartnotes.models.Note;

import static sundriver.ru.smartnotes.Constants.ID;
import static sundriver.ru.smartnotes.Constants.IMAGE_PATH;
import static sundriver.ru.smartnotes.Constants.NOTE;
import static sundriver.ru.smartnotes.Constants.PRIORITY;
import static sundriver.ru.smartnotes.Constants.TITLE;

public class MainActivity extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private Button addNoteButton;
    private ListView noteListView;
    private Database database;
    private NotesCursorAdapter cursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = new Database(this);
        setContentView(R.layout.activity_main);
        addNoteButton = (Button) findViewById(R.id.addButton);
        addNoteButton.setOnClickListener(onClickListener);

        noteListView = getListView();
        noteListView.setOnItemClickListener(onItemClickListener);

        Cursor notesCursor=database.getAllNotes();
        cursorAdapter = new NotesCursorAdapter(this,notesCursor,0);

        setListAdapter(cursorAdapter);

        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().getLoader(0).forceLoad();
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, AddEditActivity.class);
            startActivity(intent);
        }
    };

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final long rowId = id;
            final Intent intent = new Intent(MainActivity.this, ShowActivity.class);
            new AsyncTask<Object, Object, Note>() {
                @Override
                protected Note doInBackground(Object... params) {
                    return database.getNote(rowId);
                }

                @Override
                protected void onPostExecute(Note note) {
                    super.onPostExecute(note);
                    Utils.putNote(note,intent);
                    startActivity(intent);
                }
            }.execute();

        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CustomCursorLoader(this, database);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        cursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

}
