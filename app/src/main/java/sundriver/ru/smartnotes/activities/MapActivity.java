package sundriver.ru.smartnotes.activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import sundriver.ru.smartnotes.Utils;
import sundriver.ru.smartnotes.Database;
import sundriver.ru.smartnotes.R;
import sundriver.ru.smartnotes.models.Note;

import static sundriver.ru.smartnotes.Constants.*;

/**
 * Created by Eugen on 02.03.2017.
 */

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String MARKER = "Marker";
    private Database database;
    private Marker showedMarker;

    private GoogleMap map;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.map_layout);
        database = new Database(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.marker_info_layout, null, false);
                Long rowId = (Long) marker.getTag();
                final Note note = database.getNote(rowId);
                ((TextView) layout.findViewById(R.id.titleTextView)).setText(note.getTitle());
                ((TextView) layout.findViewById(R.id.noteTextView)).setText(note.getNote());
                return layout;
            }
        });
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Note note = database.getNote((Long) marker.getTag());
                Intent intent = new Intent(MapActivity.this, ShowActivity.class);
                Utils.putNote(note,intent);
                startActivity(intent);
            }
        });
        Cursor notes = database.getAllNotes();
        for (int i = 0; i < notes.getCount(); i++) {
            Long id = notes.getLong(notes.getColumnIndex(Database.ID_COLUMN));
            String coord = notes.getString(notes.getColumnIndex(Database.COORDINATES_COLUMN));
            if (coord != null && !coord.isEmpty()) {
                LatLng latLng = Utils.getPosition(coord);
                map.addMarker(new MarkerOptions().position(latLng)).setTag(id);
            }
            notes.moveToNext();
        }
        LatLng noteCoords = Utils.getPosition(getIntent().getExtras().getString(COORDINATES));
        showedMarker = map.addMarker(new MarkerOptions().position(noteCoords).title(MARKER));
        showedMarker.setTag(getIntent().getExtras().getLong(ID));
        map.moveCamera(CameraUpdateFactory.newLatLng(noteCoords));
    }


}
