package sundriver.ru.smartnotes.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import sundriver.ru.smartnotes.R;

import static sundriver.ru.smartnotes.Constants.COORDINATES;


/**
 * Created by Eugen on 02.03.2017.
 */

public class SelectMapPositionActivity extends FragmentActivity implements OnMapReadyCallback {
    private Marker marker;
    private GoogleMap map;
    private Button button;
    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.select_position_layout);
        button= (Button) findViewById(R.id.finishButton);
        SupportMapFragment mapFragment= (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        findViewById(R.id.finishButton).setOnClickListener(buttonListener);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;
        map.setOnMapClickListener(mapClickListener);

    }

    GoogleMap.OnMapClickListener mapClickListener=new GoogleMap.OnMapClickListener() {
        @Override
        public void onMapClick(LatLng latLng) {
            if(marker!=null){
                marker.remove();
                button.setClickable(true);

            }
            marker=map.addMarker(new MarkerOptions().position(latLng).title("Метка"));
            map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    };

    View.OnClickListener buttonListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(marker!=null) {
                String coords=marker.getPosition().latitude+","+marker.getPosition().longitude;
                Intent intent=new Intent();
                intent.putExtra(COORDINATES,coords);
                setResult(RESULT_OK,intent);
                finish();
            }
        }
    };
}
