package sundriver.ru.smartnotes.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.File;

import sundriver.ru.smartnotes.Utils;
import sundriver.ru.smartnotes.Database;
import sundriver.ru.smartnotes.R;
import sundriver.ru.smartnotes.models.Note;

import static sundriver.ru.smartnotes.Constants.COORDINATES;
import static sundriver.ru.smartnotes.Constants.ID;
import static sundriver.ru.smartnotes.Constants.IMAGE_PATH;
import static sundriver.ru.smartnotes.Constants.NOTE;
import static sundriver.ru.smartnotes.Constants.PRIORITY;
import static sundriver.ru.smartnotes.Constants.TITLE;

/**
 * Created by Eugen on 24.02.2017.
 */

public class AddEditActivity extends Activity {

    private static final int MAP_REQUEST_CODE = 1;
    private static final int PHOTO_REQUEST_CODE = 0;


    private EditText titleEditText;
    private EditText noteEditText;
    private RadioGroup radioGroup;
    private Button pickPhotoButton;
    private Button saveButton;
    private Button selectPositionButton;
    private TextView coordinatesTextView;
    private ImageView pickedPhotoImageView;

    private String photoPath;
    private Long rowId;
    Database database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_edit_layout);
        database=new Database(this);
        initUI();
        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            rowId = extras.getLong(ID);
            titleEditText.setText(extras.getString(TITLE));
            noteEditText.setText(extras.getString(NOTE));
            String photoPath = extras.getString(IMAGE_PATH);
            if (photoPath != null)
                if (!photoPath.isEmpty()) {
                    File photoFile = new File(photoPath);
                    if (photoFile.exists()) {
                        Bitmap bitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
                        pickedPhotoImageView.setImageBitmap(bitmap);
                    }
                }
            setPriorityRadioButton(extras.getInt(PRIORITY));
        }
    }

    private void initUI() {
        titleEditText = (EditText) findViewById(R.id.titleEditText);
        noteEditText = (EditText) findViewById(R.id.noteEditText);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        pickPhotoButton = (Button) findViewById(R.id.pickPhotoButton);
        pickPhotoButton.setOnClickListener(pickPhotoListener);
        pickedPhotoImageView = (ImageView) findViewById(R.id.pickedPhotoImageView);
        coordinatesTextView= (TextView) findViewById(R.id.coordinatesTextView);
        saveButton = (Button) findViewById(R.id.saveButton);
        selectPositionButton = (Button) findViewById(R.id.selectPositionButton);
        selectPositionButton.setOnClickListener(selectViewListener);
        saveButton.setOnClickListener(onClickListener);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!checkError()) {
                final AsyncTask<Object, Object, Object> saveTask = new AsyncTask<Object, Object, Object>() {
                    @Override
                    protected Object doInBackground(Object... params) {
                        saveNote();
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        super.onPostExecute(o);
                        finish();
                    }
                };
                if(getIntent().getExtras()==null) {
                    saveTask.execute((Object[]) null);
                }else{
                    AlertDialog.Builder builder=new AlertDialog.Builder(AddEditActivity.this);
                    builder.setTitle("Сохранение записи")
                            .setMessage("Вы уверены, что хотите сохранить запись?")
                            .setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    saveTask.execute((Object[]) null);
                                }
                            })
                            .setNegativeButton("Отменить", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                    builder.create().show();
                }
            }
        }
    };

    View.OnClickListener selectViewListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(AddEditActivity.this, SelectMapPositionActivity.class);
            startActivityForResult(intent, MAP_REQUEST_CODE);
        }
    };

    private boolean checkError() {
        boolean flag = false;
        if (titleEditText.getText().toString().isEmpty()) {
            flag = true;
            titleEditText.setError("Title should not be empty");
        }
        if (noteEditText.getText().toString().isEmpty()) {
            flag = true;
            noteEditText.setError("Note should not be empty");
        }
        return flag;
    }

    private void saveNote() {
        database = new Database(this);
        Note note = fillNote();
        if (getIntent().getExtras() == null) {
            database.addNote(note);
        } else {
            database.updateNote(note);
        }
    }

    private Note fillNote() {
        Note note = new Note();
        if (rowId != null)
            note.setId(rowId);
        note.setTitle(titleEditText.getText().toString());
        note.setNote(noteEditText.getText().toString());
        note.setPhotoPath(photoPath);
        int priorityId = radioGroup.getCheckedRadioButtonId();
        switch (priorityId) {
            case R.id.noPriorityRadioButton:
                note.setPriority(-1);
                break;
            case R.id.lowPriorityRadioButton:
                note.setPriority(0);
                break;
            case R.id.middlePriorityRadioButton:
                note.setPriority(1);
                break;
            case R.id.hightPriorityRadioButton:
                note.setPriority(2);
                break;
        }
        note.setCoordinates(coordinatesTextView.getText().toString());
        return note;
    }

    private void setPriorityRadioButton(int priority) {
        switch (priority) {
            case 0:
                radioGroup.check(R.id.lowPriorityRadioButton);
                break;
            case 1:
                radioGroup.check(R.id.middlePriorityRadioButton);
                break;
            case 2:
                radioGroup.check(R.id.hightPriorityRadioButton);
                break;
        }
    }

    View.OnClickListener pickPhotoListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, PHOTO_REQUEST_CODE);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PHOTO_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri path = data.getData();
                    pickedPhotoImageView.setImageURI(path);
                    photoPath = Utils.getRealPathFromURI(this, path);
                }
                break;
            case MAP_REQUEST_CODE:
                if (resultCode==RESULT_OK){
                    String coords=data.getExtras().getString(COORDINATES);
                    coordinatesTextView.setText(coords);
                }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
