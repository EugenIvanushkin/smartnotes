package sundriver.ru.smartnotes.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;

import java.io.File;

import retrofit2.Call;
import sundriver.ru.smartnotes.Utils;
import sundriver.ru.smartnotes.Database;
import sundriver.ru.smartnotes.NoteWriterHelper;
import sundriver.ru.smartnotes.R;
import sundriver.ru.smartnotes.models.Note;

import static sundriver.ru.smartnotes.Constants.COORDINATES;
import static sundriver.ru.smartnotes.Constants.ID;
import static sundriver.ru.smartnotes.Constants.IMAGE_PATH;
import static sundriver.ru.smartnotes.Constants.NOTE;
import static sundriver.ru.smartnotes.Constants.PRIORITY;
import static sundriver.ru.smartnotes.Constants.TITLE;

/**
 * Created by Eugen on 24.02.2017.
 */

public class ShowActivity extends Activity {

    private TextView titleTextView;
    private TextView noteTextView;
    private ImageView photoImageView;
    private TextView coordinatesTextView;

    private Database database;
    private Note note;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_note_layout);
        database = new Database(this);
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AsyncTask<Long, Object, Note>() {
            @Override
            protected Note doInBackground(Long... params) {
                return database.getNote(params[0]);
            }

            @Override
            protected void onPostExecute(Note note) {
                super.onPostExecute(note);
                fillUI(note);
            }
        }.execute(getIntent().getExtras().getLong(ID));
    }

    private void fillUI(Note note) {
        this.note = note;
        titleTextView.setText(note.getTitle());
        noteTextView.setText(note.getNote());
        LinearLayout priorityLayout = (LinearLayout) findViewById(R.id.priorityLayout);
        TextView priorityTextView = (TextView) priorityLayout.findViewById(R.id.priorityTextView);
        ImageView priorityImageView = (ImageView) priorityLayout.findViewById(R.id.priorityImageView);
        switch (note.getPriority()) {
            case -1:
                priorityTextView.setText("Отсутствует");
                priorityImageView.setVisibility(View.GONE);
                break;
            case 0:
                priorityTextView.setText("Низкий");
                priorityImageView.setImageResource(R.drawable.low_priority);
                break;
            case 1:
                priorityTextView.setText("Средний");
                priorityImageView.setImageResource(R.drawable.medium_priority);
                break;
            case 2:
                priorityTextView.setText("Высокий");
                priorityImageView.setImageResource(R.drawable.critical_priority);
                break;
        }
        String photo = note.getPhotoPath();
        if (photo != null && !photo.isEmpty()) {
            File photoFile = new File(photo);
            if (photoFile.exists()) {
                Bitmap bitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
                photoImageView.setImageBitmap(bitmap);
            }
        } else {
            photoImageView.setVisibility(View.GONE);
        }
        coordinatesTextView.setText(note.getCoordinates());

    }

    private void initUI() {
        titleTextView = (TextView) findViewById(R.id.titleTextView);
        noteTextView = (TextView) findViewById(R.id.noteTextView);
        photoImageView = (ImageView) findViewById(R.id.photoImageView);
        coordinatesTextView = (TextView) findViewById(R.id.coordinatesTextView);
        findViewById(R.id.editButton).setOnClickListener(editClickListener);
        findViewById(R.id.deleteButton).setOnClickListener(deleteClickListener);
        findViewById(R.id.importNoteButton).setOnClickListener(importListener);
        findViewById(R.id.coordinatesViewButton).setOnClickListener(viewMapListener);
    }

    View.OnClickListener editClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ShowActivity.this, AddEditActivity.class);
            Utils.putNote(note,intent);
            startActivity(intent);
        }
    };

    View.OnClickListener deleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder=new AlertDialog.Builder(ShowActivity.this);
            builder.setTitle("Удаление записи")
                    .setMessage("Удалить запись?")
                    .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new AsyncTask<Long, Object, Object>() {
                                @Override
                                protected Object doInBackground(Long... params) {
                                    database.deleteNote(note.getId());
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Object o) {
                                    super.onPostExecute(o);
                                    finish();
                                }
                            }.execute();
                        }
                    })
                    .setNegativeButton("Отменить", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            builder.create().show();

        }
    };

    View.OnClickListener importListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String path = NoteWriterHelper.writeNote(note);
            if (path != null) {
                Toast.makeText(ShowActivity.this, "Запись экспортирована успешно:" + path, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(ShowActivity.this, "Что-то пошло не так...", Toast.LENGTH_LONG).show();
            }
        }
    };

    View.OnClickListener twitterListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
            TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient(session);
            StatusesService statusesService = twitterApiClient.getStatusesService();
            String tweet = Utils.generateMessage(note);
            String coords = "7238f93a3e899af6";
            Call<Tweet> call = statusesService.update(tweet, null, null, null, null, coords, null, null, null);
            call.enqueue(new Callback<Tweet>() {
                @Override
                public void success(Result<Tweet> result) {
                    Toast.makeText(ShowActivity.this, R.string.success_tweet, Toast.LENGTH_LONG).show();
                }

                @Override
                public void failure(TwitterException exception) {
                    Toast.makeText(ShowActivity.this, R.string.failure_tweet, Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    View.OnClickListener viewMapListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(note.getCoordinates()==null || note.getCoordinates().isEmpty()){
                Toast.makeText(ShowActivity.this,"Координаты отсутствуют",Toast.LENGTH_LONG).show();
                return;
            }
            Intent intent = new Intent(ShowActivity.this, MapActivity.class);
            intent.putExtra(ID, note.getId());
            intent.putExtra(COORDINATES, note.getCoordinates());
            startActivity(intent);
        }
    };
}
