package sundriver.ru.smartnotes;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.Date;

import sundriver.ru.smartnotes.models.Note;

import static sundriver.ru.smartnotes.Constants.COORDINATES;
import static sundriver.ru.smartnotes.Constants.ID;
import static sundriver.ru.smartnotes.Constants.IMAGE_PATH;
import static sundriver.ru.smartnotes.Constants.NOTE;
import static sundriver.ru.smartnotes.Constants.PRIORITY;
import static sundriver.ru.smartnotes.Constants.TITLE;

/**
 * Created by Eugen on 01.03.2017.
 */

public class Utils {

    public static String getPriority(int priority){
        String res=null;
        switch (priority){
            case 0:
                res="Низкий";
                break;
            case 1:
                res="Нормальный";
                break;
            case 2:
                res="Высокий";
                break;
        }
        return res;
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static String generateMessage(Note note){
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd:MM:yyyy , HH:mm");
        StringBuilder stringBuilder=new StringBuilder();

        stringBuilder.append(dateFormat.format(new Date()));
        stringBuilder.append("\n");

        stringBuilder.append(note.getTitle());
        stringBuilder.append("\n");

        stringBuilder.append(note.getNote());
        stringBuilder.append("\n");;

        stringBuilder.append(Utils.getPriority(note.getPriority()));
        stringBuilder.append("\n");

        stringBuilder.append(note.getCoordinates());
        stringBuilder.append("\n");

        String photoPath=note.getPhotoPath();
        if(photoPath!=null
                && !photoPath.isEmpty()) {
            stringBuilder.append(photoPath);
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public static LatLng getPosition(String coord){
        String[] arr=coord.split(",");
        LatLng res=new LatLng(Double.parseDouble(arr[0]),Double.parseDouble(arr[1]));
        return res;
    }

    public static void putNote(Note note,Intent intent){
        intent.putExtra(ID, note.getId());
        intent.putExtra(TITLE, note.getTitle());
        intent.putExtra(NOTE, note.getNote());
        intent.putExtra(PRIORITY, note.getPriority());
        intent.putExtra(IMAGE_PATH, note.getPhotoPath());
        intent.putExtra(COORDINATES,note.getCoordinates());
    }
}
