package sundriver.ru.smartnotes;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import sundriver.ru.smartnotes.fragments.ShowImageFragment;

/**
 * Created by Eugen on 14.03.2017.
 */

public class NotesCursorAdapter extends CursorAdapter {
    private LayoutInflater inflater;
    private Context context;
    public NotesCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.context=context;
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(R.layout.list_item,null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ImageView imageView= (ImageView) view.findViewById(R.id.photoImageView);
        ImageView priorityImageView= (ImageView) view.findViewById(R.id.priorityImageView);
        TextView titleTextView= (TextView) view.findViewById(R.id.noteTitleTextView);

        String imagePath=cursor.getString(cursor.getColumnIndex(Database.PHOTO_COLUMN));
        String title=cursor.getString(cursor.getColumnIndex(Database.TITLE_COLUMN));
        int priority=cursor.getInt(cursor.getColumnIndex(Database.PRIORITY_COLUMN));
        if (imagePath != null) {
            File photoFile = new File(imagePath);
            Bitmap bitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
            imageView.setImageBitmap(bitmap);
        }else{
            imageView.setImageResource(R.drawable.image_not_found);
        }
        imageView.setTag(imagePath);
        titleTextView.setText(title);
        switch (priority) {
            case 0:
                priorityImageView.setImageResource(R.drawable.low_priority);
                break;
            case 1:
                priorityImageView.setImageResource(R.drawable.medium_priority);
                break;
            case 2:
                priorityImageView.setImageResource(R.drawable.critical_priority);
                break;
        }
        imageView.setOnClickListener(imageClickListener);
    }
    View.OnClickListener imageClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String imagePath= (String) v.getTag();
            if(imagePath==null || imagePath.isEmpty())
                return;
            Bundle args=new Bundle();
            args.putString("imagePath",imagePath);

            ShowImageFragment fragment=new ShowImageFragment();
            fragment.setArguments(args);
            fragment.show(((Activity)context).getFragmentManager(),"showDialog");
        }
    };
}
