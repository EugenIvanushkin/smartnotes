package sundriver.ru.smartnotes.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;

import sundriver.ru.smartnotes.R;

/**
 * Created by Eugen on 14.03.2017.
 */

public class ShowImageFragment extends DialogFragment {

    private ImageView imageView;
    String path;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        path=getArguments().getString("imagePath");
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v=inflater.inflate(R.layout.fragment_show_image,null);
        imageView= (ImageView) v.findViewById(R.id.imageView);
        File imageFile=new File(path);
        if(imageFile.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            imageView.setImageBitmap(bitmap);
        }else{
            imageView.setImageResource(R.drawable.image_not_found);
        }
        return v;
    }
}
