package sundriver.ru.smartnotes;

/**
 * Created by Eugen on 17.03.2017.
 */

public  class Constants {
    public static final String IMAGE_PATH="imagePath";
    public static final String TITLE="title";
    public static final String NOTE="note";
    public static final String ID="id";
    public static final String PRIORITY="priority";
    public static final String COORDINATES="coordinates";
}
