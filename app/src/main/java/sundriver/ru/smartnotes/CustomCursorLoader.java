package sundriver.ru.smartnotes;


import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;


/**
 * Created by Eugen on 24.02.2017.
 */

public class CustomCursorLoader extends CursorLoader {
    private Database database;
    public CustomCursorLoader(Context context,Database database) {
        super(context);
        this.database=database;
    }
    @Override
    public Cursor loadInBackground(){
        return database.getAllNotes();
    }
}
